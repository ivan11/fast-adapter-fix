package iph.fastadapterfix;

import android.support.annotation.StringRes;

/**
 * Created by ivanphytsyk on 4/27/17.
 */


public class StringHolder extends com.mikepenz.materialize.holder.StringHolder {
    public StringHolder(String text) {
        super(text);
    }

    public StringHolder(@StringRes int textRes) {
        super(textRes);
    }
}
