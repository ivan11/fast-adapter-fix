package iph.fastadapterfix.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.mopub.nativeads.MoPubRecyclerAdapter;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;
import com.timehop.stickyheadersrecyclerview.caching.HeaderViewCache;
import com.timehop.stickyheadersrecyclerview.util.OrientationProvider;

/**
 * Created by ivanphytsyk on 4/29/17.
 */

public class MyHeaderViewCache extends HeaderViewCache
{
    private MoPubRecyclerAdapter moPubRecyclerAdapter;

    public MyHeaderViewCache(StickyRecyclerHeadersAdapter adapter, OrientationProvider orientationProvider) {
        super(adapter, orientationProvider);
    }

    public MyHeaderViewCache(StickyRecyclerHeadersAdapter adapter, MoPubRecyclerAdapter moPubRecyclerAdapter,  OrientationProvider orientationProvider) {
        super(adapter, orientationProvider);
        this.moPubRecyclerAdapter = moPubRecyclerAdapter;
    }

    @Override
    public View getHeader(RecyclerView parent, int position) {
        int originalPosition = moPubRecyclerAdapter.getOriginalPosition(position);
        if (originalPosition < 0) {
            if (position == 0) {
                originalPosition = 0;
            } else {
                originalPosition = moPubRecyclerAdapter.getOriginalPosition(position - 1);
            }
        }
        return super.getHeader(parent, originalPosition);
    }
}
