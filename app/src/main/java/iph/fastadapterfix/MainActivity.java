package iph.fastadapterfix;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.adapters.HeaderAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.mopub.nativeads.MoPubRecyclerAdapter;
import com.mopub.nativeads.MoPubStaticNativeAdRenderer;
import com.mopub.nativeads.ViewBinder;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.ArrayList;
import java.util.List;

import iph.fastadapterfix.adapters.MopubFastItemAdapter;
import iph.fastadapterfix.adapters.MyStickyRecyclerHeadersDecoration;
import iph.fastadapterfix.adapters.StickyHeaderAdapter;

public class MainActivity extends AppCompatActivity {
    private static final String[] headers = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MopubFastItemAdapter fastAdapter =  new MopubFastItemAdapter<>();

        fastAdapter.withSelectable(true);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv);

        final StickyHeaderAdapter stickyHeaderAdapter = new StickyHeaderAdapter();
        final HeaderAdapter headerAdapter = new HeaderAdapter();

        ViewBinder viewBinder = new ViewBinder.Builder(R.layout.native_ad_item)
                .iconImageId(R.id.native_icon_image)
                .titleId(R.id.native_title)
                .textId(R.id.native_text)
                .callToActionId(R.id.native_cta)
                .privacyInformationIconImageId(R.id.native_privacy_information_icon_image)
                .build();

        MoPubRecyclerAdapter adapter = new MoPubRecyclerAdapter(this, stickyHeaderAdapter.wrap(headerAdapter.wrap(fastAdapter)));
        adapter.registerAdRenderer(new MoPubStaticNativeAdRenderer(viewBinder));
        adapter.loadAds("76a3fefaced247959582d2d2df6f4757");

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        fastAdapter.withMoPubAdAdapter(adapter);

        recyclerView.setAdapter(adapter);

        final MyStickyRecyclerHeadersDecoration decoration = new MyStickyRecyclerHeadersDecoration(stickyHeaderAdapter, adapter);
        recyclerView.addItemDecoration(decoration);

        //fill with some sample data
        List<IItem> items = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            items.add(new SimpleItem().withName("Test " + i).withHeader(headers[i / 5]).withIdentifier(100 + i));
        }
        fastAdapter.add(items);

        //so the headers are aware of changes
        stickyHeaderAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                decoration.invalidateHeaders();
            }
        });

        //restore selections (this has to be done after the items were added
        fastAdapter.withSavedInstanceState(savedInstanceState);
    }
}
